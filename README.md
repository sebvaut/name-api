# Name API
API to provide names!

Names are sorted by category


## API Definiton
**Base path:** /v1/names

| Method | Path | Description |
|---|---|---|
| GET | /categories/all | Returns a list of categories |
| GET | /categories/\<category\>/all | Returns a list of names for a specific category |
| GET | /categories/\<category\>/random | Returns a random name from a category |
| GET | /categories/random | Returns a random category |


### Accept Content Type
The following content types can be returned:
 * text/plain
 * application/json
 * application/xml

 The "Accept" header can be used in the request to specify the returned content type. The default is text/plain.

### Pretty Print
For application encoded content types (json and xml), the "pretty=true" URL param can be used to return an indented result.

## Example
The following returns a random name of the space category in JSON format
```
$ curl -H "Accept: application/json" "http://api.thesebnet.com/v1/names/categories/space/random?pretty=1"
{
  "value": "pathfinder"
}

```