package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func newServer(addr string, nameSet ...NameSet) *server {
	return &server{
		addr:     addr,
		nameSets: nameSet,
	}
}

// Server is the main struct of the server
type server struct {
	addr     string
	nameSets []NameSet
	router   *mux.Router
}

func (s *server) run() error {
	s.routes()
	fmt.Println("server running on addr " + s.addr)
	return http.ListenAndServe(s.addr, s.router)
}
