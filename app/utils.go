package main

import (
	"math/rand"
	"time"
)

func (s *server) Ran(items []string) string {
	rand.Seed(time.Now().UnixNano())

	l := len(items)
	if l == 0 {
		return ""
	}
	return items[rand.Intn(len(items))]
}
