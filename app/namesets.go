package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"path"
	"sort"
	"strings"
)

func loadNameSets(filepath string) (nameSets []NameSet, err error) {
	files, err := ioutil.ReadDir(filepath)
	if err != nil {
		return nil, errors.New("Error reading path " + filepath + ": " + err.Error())
	}
	for _, f := range files {
		if f.IsDir() {
			continue
		}
		if strings.HasSuffix(f.Name(), ".json") {
			n, err := nameSetFromJSONFile(path.Join(filepath, f.Name()))
			if err != nil {
				log.Println("WARN:", err)
				continue
			}

			cleanNameSet(&n)

			nameSets = append(nameSets, n)
		}
	}
	if len(nameSets) == 0 {
		return nameSets, errors.New("No namesets found in path " + filepath)
	}
	return nameSets, nil
}

func nameSetFromJSONFile(filename string) (n NameSet, err error) {
	f, err := os.Open(filename)
	if err != nil {
		return n, errors.New("can't open " + filename + " for nameSets load: " + err.Error())
	}
	defer f.Close()
	j := json.NewDecoder(f)

	err = j.Decode(&n)
	if err != nil {
		return n, errors.New("can't decode json " + filename + " for nameSets load: " + err.Error())
	}
	return n, nil
}

func cleanNameSet(nameSet *NameSet) {
	// category should be lowercase without spaces
	nameSet.Category = strings.ToLower(nameSet.Category)
	nameSet.Category = strings.Replace(nameSet.Category, " ", "", -2)

	// Title casing for names + dedup
	newNames := make(map[string]struct{})
	for _, n := range nameSet.Names {
		cleanedN := strings.ToLower(n)
		if _, seen := newNames[cleanedN]; !seen {
			newNames[cleanedN] = struct{}{}
		}
	}

	// re-create nameset.Names slice with de-dupped+cleaned
	nameSet.Names = make([]string, len(newNames))
	i := 0
	for n := range newNames {
		nameSet.Names[i] = n
		i++
	}

	// sort
	sort.Strings(nameSet.Names)
}

// NameSet defines a category and the list of names for that category
type NameSet struct {
	Category string   `json:"category"`
	Names    []string `json:"names"`
}

func (s *server) Categories() []string {
	c := make([]string, len(s.nameSets))
	for i, n := range s.nameSets {
		c[i] = n.Category
	}
	return c
}

func (s *server) Names(cat string) ([]string, error) {
	for _, n := range s.nameSets {
		if strings.ToLower(n.Category) == strings.ToLower(cat) {
			return n.Names, nil
		}
	}
	return nil, errors.New("Category not found")
}
