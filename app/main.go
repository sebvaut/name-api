package main

import (
	"flag"
	"log"
)

var nameSetPath string
var addr string

func init() {
	flag.StringVar(&addr, "addr", ":8080", "webserver address")
	flag.StringVar(&nameSetPath, "namesets", "/namesets", "path to nameset JSON files")
	flag.Parse()
}

func main() {
	// get namesets
	nameSets, err := loadNameSets(nameSetPath)
	if err != nil {
		log.Fatalln(err)
	}
	s := newServer(addr, nameSets...)
	log.Fatalln(s.run())
}
