package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

const apiPrefix = "/v1/names"

// ValueResponse is the struct used whenever a single value is sent
type ValueResponse struct {
	Value string `json:"value"`
}

func (v ValueResponse) String() string {
	return v.Value
}

// ValuesResponse is the struct used whenever a list of values is sent
type ValuesResponse struct {
	Values []string `json:"values"`
}

func (v ValuesResponse) String() string {
	return strings.Join(v.Values, ",")
}

// Value is any type returned
type Value interface {
	String() string
}

func (s *server) routes() {
	s.router = mux.NewRouter()
	api := s.router.PathPrefix(apiPrefix).Subrouter()

	api.HandleFunc("/categories/all", s.handleGetCategories).Methods(http.MethodGet)
	api.HandleFunc("/categories/random", s.handleGetRanCat).Methods(http.MethodGet)
	api.HandleFunc("/categories/{category}/all", s.handleGetCategoriesNames).Methods(http.MethodGet)
	api.HandleFunc("/categories/{category}/random", s.handleGetCategorieRanNames).Methods(http.MethodGet)

}

func (s *server) handleGetCategories(w http.ResponseWriter, r *http.Request) {
	s.serveContent(ValuesResponse{s.Categories()}, w, r)
}
func (s *server) handleGetRanCat(w http.ResponseWriter, r *http.Request) {
	s.serveContent(ValueResponse{s.Ran(s.Categories())}, w, r)
}

func (s *server) handleGetCategoriesNames(w http.ResponseWriter, r *http.Request) {
	cat, ok := mux.Vars(r)["category"]
	if !ok {
		http.Error(w, "category param not provided", http.StatusBadRequest)
		return
	}
	names, err := s.Names(cat)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	s.serveContent(ValuesResponse{names}, w, r)
}

func (s *server) handleGetCategorieRanNames(w http.ResponseWriter, r *http.Request) {
	cat, ok := mux.Vars(r)["category"]
	if !ok {
		http.Error(w, "category param not provided", http.StatusBadRequest)
		return
	}
	names, err := s.Names(cat)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	s.serveContent(ValueResponse{s.Ran(names)}, w, r)
}

func (s *server) serveContent(v Value, w http.ResponseWriter, r *http.Request) {
	switch r.Header.Get("Accept") {
	case "application/json":
		j := json.NewEncoder(w)
		if r.FormValue("pretty") != "" {
			j.SetIndent("", "  ")
		}
		err := j.Encode(v)
		if err != nil {
			http.Error(w, `{"error":"failed to parse json result"}`, http.StatusInternalServerError)
			return
		}
	case "application/xml":
		x := xml.NewEncoder(w)
		if r.FormValue("pretty") != "" {
			x.Indent("", "  ")
		}
		err := x.Encode(v)
		if err != nil {
			http.Error(w, `<?xml version="1.0" encoding="UTF-8"?>
			<root>
			   <error>failed to parse json result</error>
			</root>`, http.StatusInternalServerError)
			return
		}
	default:
		fmt.Fprintln(w, v.String())
	}
}
