FROM golang:1.13 AS BUILD

ENV GOOS=linux
ENV GOARCH=amd64
ENV CGO_ENABLED=0

WORKDIR /app

COPY /app/ .

RUN go mod download \
&& go build -o app .

EXPOSE 8080

FROM scratch

COPY --from=BUILD /app/app .
COPY /namesets /namesets

CMD ["./app"]  
